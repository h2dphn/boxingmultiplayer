﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingMultiplayer
{
    public static class Constants
    {
        private const string MYSQL_SERVER = "boxinggame.mysql.database.azure.com";
        private const string MYSQL_SERVER_LOCAL = "localhost";

        private const string MYSQL_DB = "boxinggame";
        private const string MYSQL_DB_LOCAL = "boxinggame";

        private const string MYSQL_PASSWORD = "Datait2019!";
        private const string MYSQL_PASSWORD_LOCAL = "Datait2019!";

        private const string MYSQL_USERNAME = "boxeradmin";
        private const string MYSQL_USERNAME_LOCAL = "boxeradmin";

        /// <summary>
        /// Combines connection data to a long string
        /// </summary>
        /// <returns>Connection string to a database</returns>
        public static string GetConString()
        {
            return Debugger.IsAttached ? 
                $"server={MYSQL_SERVER};database={MYSQL_DB};uid={MYSQL_USERNAME}@{MYSQL_DB};pwd={MYSQL_PASSWORD}" :
                $"server={MYSQL_SERVER_LOCAL};database={MYSQL_DB_LOCAL};uid={MYSQL_USERNAME_LOCAL};pwd={MYSQL_PASSWORD_LOCAL}";
        }
    }
}
