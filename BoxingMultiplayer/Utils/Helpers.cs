﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingMultiplayer.Utils
{
    public static class Helpers
    {
        public static void Output(List<UserMsg> msgs)
        {
            Console.WriteLine("------------------------");
            foreach (UserMsg msg in msgs)
            {
                Console.WriteLine(msg.Message);

            }

            var input = Console.ReadLine();

            Console.WriteLine("------------------------");
        }

        public static void Output(string msg)
        {

        }
    }
}
