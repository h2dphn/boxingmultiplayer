﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoxingMultiplayer.Utils
{
    public class UserMsg
    {
        public string Message { get; set; }
        public string Input { get; set; }
        public object ExecuteFunction { get; set; }
        public UserMsg()
        {
            
        }
    }
}
